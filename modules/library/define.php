<?php

/**
 * general running mode
 */
defined('MODULES_MODE_DEVELOPMENT') ||
    define('MODULES_MODE_DEVELOPMENT', 1);

defined('MODULES_MODE_STAGING') ||
    define('MODULES_MODE_STAGING', 2);

defined('MODULES_MODE_PRODUCTION') ||
    define('MODULES_MODE_PRODUCTION', 3);

/**
 * sql server selection
 */
defined('MODULES_USE_MYSQL') ||
    define('MODULES_USE_MYSQL', 1);

defined('MODULES_USE_PGSQL') ||
    define('MODULES_USE_PGSQL', 2);

defined('MODULES_USE_SQLITE') ||
    define('MODULES_USE_SQLITE', 3);

/**
 * directory specific
 */
defined('MODULES_DIR') ||
    define('MODULES_DIR', dirname(__DIR__));

defined('MODULES_LIBRARY') ||
    define('MODULES_LIBRARY', MODULES_DIR . DIRECTORY_SEPARATOR .'library');

defined('GENERAL_DIR') ||
    define('GENERAL_DIR', MODULES_DIR . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR . 'general');
