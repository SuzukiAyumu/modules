<?php
/**
 * setup auto loader
 */
class Loader
{
    public static function defaultLoader($className)
    {
        preg_match_all('/([A-Z][^A-Z]+)/sm', $className, $classNameMatches);
        
        $sep = DIRECTORY_SEPARATOR;
        if ($className === 'Modules') {
            $require_url = MODULES_DIR . $sep . 'module' .$sep . 'modules.php';
            require_once($require_url);
            return;
        }
        switch ($classNameMatches[0][0]) {
            case "Modules":
                $splitClasName = array_shift($classNameMatches);
                array_shift($splitClasName);
                $require_url = MODULES_DIR . $sep . 'module' .$sep . strtolower(join($sep, $splitClasName) . '.php');
                require_once($require_url);
            break;
        };
    }

    public static function setup()
    {
        spl_autoload_register(array('Loader', 'defaultLoader'));
    }
}

Loader::setup();
