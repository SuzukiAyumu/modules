<?php
/**
 * iniファイル取り扱い
 *
 */
class ModulesConfigParse
{
    public static $_ini;

    /**
     * setting search by key
     *
     * @param Type $key search key name
     */
    public static function find($key)
    {
        return self::$_ini->{$key};
    }

    /**
     * iniファイルをパースしてArrayObjectで返却
     *
     * @return ModulesArrayObject Description
     */
    public static function get($fileDir, $envStatus = "develop")
    {

        /**
         * Iniファイルのパース
         */
        $fileList = "";
        if (is_dir($fileDir)) {
            $fileList = glob("{$fileDir}/{Common,{$envStatus}}/*.ini", GLOB_BRACE);
        } else {
            $fileList = array( $fileDir );
        }

        $configs  = array();
        while ($fileName = array_shift($fileList)) {
            $conf = parse_ini_file($fileName, true);
            foreach ($conf as $key=> $value) {
                $configs[$key] = $value;
            }
            unset($value);
        }

        /**
         * キーをパース
         */
        $stacBase = array();
        foreach ($configs as $name=> $list) {
            $stac = array();
            $keys = explode(".", $name);
            self::_createStac($keys, $list, $stac);

            $stacBase = array_merge_recursive($stacBase, $stac);
        }
        $configs = $stacBase;

        /**
         * 再帰的にオブジェクト化
         */
        self::$_ini = new ModulesArrayObject($configs);

        return self::$_ini;
    }

    /**
     * 配列のキーでスタック構造を作る
     */
    private static function _createStac($keys, $item, &$stac)
    {
        $name = array_shift($keys);
        $stac[ $name ] = array();
        if (0 < count($keys)) {
            self::_createStac($keys, $item, $stac[ $name ]);
        } else {
            $stac[ $name ] = $item;
        }
    }
}
