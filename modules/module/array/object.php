<?php

class ModulesArrayObject implements ArrayAccess, IteratorAggregate, Countable
{
    private static $_iteriItems = array();

    public function __construct($array)
    {
        self::$_iteriItems = $array;
        if (is_array(self::$_iteriItems)) {
            array_walk(self::$_iteriItems, array( $this, "_recrusiveProcessing" ), self::$_iteriItems);
        } else {
            self::$_iteriItems = array( self::$_iteriItems );
            array_walk(self::$_iteriItems, array( $this, "_recrusiveProcessing" ), self::$_iteriItems);
        }
    }

    public function count()
    {
        return count(self::$_iteriItems);
    }

    public function getIterator()
    {
        return new ArrayIterator(self::$_iteriItems);
    }


    public function offsetExists($offset)
    {
        return isset(self::$_iteriItems[ $offset ]);
    }


    public function offsetGet($offset)
    {
        return self::$_iteriItems[ $offset ];
    }


    public function offsetSet($offset, $value)
    {
        return self::$_iteriItems[ $offset ] = $value;
    }


    public function offsetUnset($offset)
    {
        unset(self::$_iteriItems[ $offset ]);
    }


    public function __get($name)
    {
        if (isset(self::$_iteriItems[ $name ])) {
            return self::$_iteriItems[ $name ];
        }
        return null;
    }


    public function __set($name, $value)
    {
        self::$_iteriItems[ $name ] = $value;
    }


    public function __unset($name)
    {
        unset(self::$_iteriItems[ $name ]);
    }
    
    public function __debugInfo()
    {
        return self::$_iteriItems;
    }

    public function next()
    {
        return next(self::$_iteriItems);
    }


    public function toArray()
    {
        $result = array();
        foreach (self::$_iteriItems as $k=> $p) {
            if ($p instanceof ModulesArrayObject) {
                $result[ $k ] = $p->toArray();
            } else {
                $result[ $k ] = $p;
            }
        }
        return $result;
    }


    public function getArrayCopy()
    {
        return $this->toArray();
    }


    public function current()
    {
        return current(self::$_iteriItems);
    }

    private static function _recrusiveProcessing(&$item, $key, &$items)
    {
        if (is_array($item)) {
            foreach ($item as &$citem) {
                if (is_array($citem)) {
                    $citem = new ModulesArrayObject($citem);
                }
            }
            unset($citem);

            $item = new ModulesArrayObject($item);
            $items[ $key ] = $item;
        }
    }
}
